-- Add migration script here
CREATE TABLE super_role (
	guild_id int8 NOT NULL,
	role_id int8 NOT NULL,
	CONSTRAINT super_role_pkey PRIMARY KEY (guild_id, role_id)
);