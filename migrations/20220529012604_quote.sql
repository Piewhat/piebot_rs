-- Add migration script here
CREATE TABLE "quote" (
	guild_id int8 NOT NULL,
	user_id int8 NOT NULL,
	url text NOT NULL,
	name text NOT NULL,
	"time" timestamp NULL,
	CONSTRAINT quote_pk PRIMARY KEY (name, guild_id)
);