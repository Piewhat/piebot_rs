-- Add migration script here
CREATE TABLE starboard (
	guild_id int8 NOT NULL,
	channel_id int8 NOT NULL,
	CONSTRAINT starboard_pk PRIMARY KEY (guild_id)
);