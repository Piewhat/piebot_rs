-- Add migration script here
CREATE TABLE marbles (
	guild_id int8 NOT NULL,
	user_id int8 NOT NULL,
	marbles int4 NOT NULL,
	CONSTRAINT marbles_pkey PRIMARY KEY (guild_id, user_id)
);