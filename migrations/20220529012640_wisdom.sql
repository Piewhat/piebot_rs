-- Add migration script here
CREATE TABLE wisdom (
	guild_id int8 NOT NULL,
	user_id int8 NOT NULL,
	phrase text NOT NULL,
	"time" timestamp NULL,
	CONSTRAINT wisdom_pkey PRIMARY KEY (user_id, guild_id, phrase)
);