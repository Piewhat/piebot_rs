-- Add migration script here
CREATE TABLE role_commands (
	guild_id int8 NOT NULL,
	role_id int8 NOT NULL,
	command text NOT NULL,
	CONSTRAINT role_commands_pkey PRIMARY KEY (guild_id, role_id, command)
);