-- Add migration script here
CREATE TABLE disabled_commands (
	guild_id int8 NOT NULL,
	command text NOT NULL,
	CONSTRAINT disabled_commands_pkey PRIMARY KEY (guild_id, command)
);