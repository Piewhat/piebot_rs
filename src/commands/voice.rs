use std::{sync::Arc, time::Duration};

use crate::{Context, Error};
use futures::{Stream, StreamExt};
use poise::{
    serenity_prelude::{self as serenity, ChannelId, User, UserId},
    Modal,
};
use songbird::error::ConnectionResult;

#[poise::command(
    slash_command,
    description_localized("en-US", "Play any url"),
    guild_only
)]
pub async fn p(
    ctx: Context<'_>,
    #[description = "Youtube search or URL"] mut query: String,
) -> Result<(), Error> {
    ctx.defer().await?;
    query = parse_query(&query).await?;
    let source = songbird::ytdl(&query).await?;
    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    {
        let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
        let mut handler = handler_lock.lock().await;
        handler.play_source(source);
    }

    ctx.say(format!("Playing {}", query)).await?;

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Queue any playable url"),
    guild_only
)]
pub async fn pq(
    ctx: Context<'_>,
    #[description = "Youtube search or URL"] mut query: String,
) -> Result<(), Error> {
    ctx.defer().await?;
    query = parse_query(&query).await?;
    let source = songbird::ytdl(&query).await?;
    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    {
        let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
        let mut handler = handler_lock.lock().await;
        handler.enqueue_source(source);
    }

    ctx.say(format!("Added {} to queue", query)).await?;

    Ok(())
}

async fn parse_query(query: &str) -> Result<String, Error> {
    let mut query = query.to_owned();
    if !query.starts_with("http") {
        let key =
            std::env::var("GOOGLE_API_KEY").expect("Expected a GOOGLE_API_KEY in the environment");
        let endpoint = &format!("https://www.googleapis.com/youtube/v3/search?type=video&q={}&maxResults=1&part=snippet&key={}", query, key);

        let search_results = reqwest::Client::new()
            .get(endpoint)
            .send()
            .await?
            .json::<serde_json::Value>()
            .await?;

        if let Some(id) = search_results["items"][0]["id"]["videoId"].as_str() {
            query = format!("https://www.youtube.com/watch?v={}", id);
        } else {
            return Err("Youtube API search result was empty".into());
        }
    }

    Ok(query)
}

#[derive(poise::Modal)]
#[name = "Queue"]
struct QueueModal {
    #[name = "Each line can be a search or link"]
    #[paragraph]
    queue: String,
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Start the queue"),
    guild_only,
    rename = "q",
    subcommands("q_add", "q_skip")
)]
pub async fn queue(ctx: Context<'_>) -> Result<(), Error> {
    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Add links or Youtube searches"),
    guild_only,
    rename = "add"
)]
pub async fn q_add(ctx: Context<'_>) -> Result<(), Error> {
    let queue = if let Context::Application(ctx) = ctx {
        if let Some(data) = QueueModal::execute(ctx).await? {
            data.queue
        } else {
            return Ok(());
        }
    } else {
        return Ok(());
    };

    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
    let mut handler = handler_lock.lock().await;
    let queue_vec = queue.split("\n");
    for q in queue_vec {
        let query = parse_query(q).await?;
        let source = songbird::ytdl(&query).await?;
        handler.enqueue_source(source);
    }

    Ok(())
}

#[poise::command(slash_command, guild_only, rename = "skip")]
pub async fn q_skip(ctx: Context<'_>) -> Result<(), Error> {
    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };
    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
    let handler = handler_lock.lock().await;
    handler.queue().skip()?;
    ctx.reply("Skipped").await?;

    Ok(())
}
#[poise::command(
    slash_command,
    description_localized("en-US", "Play audio files"),
    guild_only
)]
pub async fn pfile(ctx: Context<'_>, file: serenity::Attachment) -> Result<(), Error> {
    let source = songbird::ytdl(&file.url).await?;
    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    {
        let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
        let mut handler = handler_lock.lock().await;
        handler.play_source(source);
    }

    ctx.say(format!("Playing {}", file.filename)).await?;

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Stop playing"),
    guild_only
)]
pub async fn dc(ctx: Context<'_>) -> Result<(), Error> {
    let reply = ctx.say("Attempting to stop").await?;
    let guild_id = ctx.guild_id().unwrap();
    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    if let Some(handler_lock) = manager.get(guild_id) {
        let mut handler = handler_lock.lock().await;
        handler.stop();
        reply.edit(ctx, |m| m.content("Stopped")).await?;
    } else {
        reply.edit(ctx, |m| m.content("Failed to stop")).await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Leave the channel"),
    guild_only
)]
pub async fn leave(ctx: Context<'_>) -> Result<(), Error> {
    let guild_id = ctx.guild_id().unwrap();

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    manager.remove(guild_id).await?;
    ctx.say("Goodbye").await?;

    Ok(())
}

pub async fn autocomplete_quote<'a>(
    ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    let guild_id = ctx.guild_id().unwrap();
    let quotes = Arc::clone(&ctx.data().quotes);
    let quotes = quotes.read().await;
    let quotes = quotes.get(&guild_id.0).map_or(vec![], |x| x.to_owned());

    futures::stream::iter(quotes).filter(move |name| {
        futures::future::ready(name.to_lowercase().contains(&partial.to_lowercase()))
    })
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Play a quote"),
    guild_only
)]
pub async fn quote(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_quote"] quote: Option<String>,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap();

    ctx.defer().await?;

    let quote = if let Some(quote) = quote {
        sqlx::query!(
            "SELECT user_id, name, url, time FROM quote WHERE guild_id = $1 AND LOWER(name) LIKE $2 LIMIT 1",
            &i64::from(guild_id),
            format!("%{}%", &quote).to_lowercase(),
        )
        .map(|r| (r.user_id, r.name, r.url, r.time))
        .fetch_optional(&pool)
        .await?
    } else {
        sqlx::query!(
            "SELECT user_id, name, url, time FROM quote WHERE guild_id = $1 ORDER BY RANDOM () LIMIT 1",
            &i64::from(guild_id),
        )
        .map(|r| (r.user_id, r.name, r.url, r.time))
        .fetch_optional(&pool)
        .await?
    };

    let quote = if let Some(quote) = quote {
        quote
    } else {
        ctx.say("I can't find that one").await?;
        return Ok(());
    };

    let (user_id, name, url, time) = quote;

    let source = songbird::ffmpeg(format!("./data/quotes/{}", &url)).await?;
    let guild_id = ctx.guild_id().unwrap();
    let channel_id = if let Some(channel_id) = ctx
        .guild()
        .unwrap()
        .voice_states
        .get(&ctx.author().id)
        .and_then(|voice_state| voice_state.channel_id)
    {
        channel_id
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let manager = songbird::get(ctx.serenity_context())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    {
        let (handler_lock, _success) = manager.join(guild_id, channel_id).await;
        let mut handler = handler_lock.lock().await;
        handler.play_source(source);
    }

    let user = ctx.http().get_user(user_id as u64).await?;

    let uuid_info = ctx.id();
    let message = ctx
        .send(|f| {
            f.content(name).components(|c| {
                if user_id == 0 {
                    return c;
                }
                c.create_action_row(|ar| {
                    ar.create_button(|b| {
                        b.style(serenity::ButtonStyle::Primary)
                            .label("Info")
                            .custom_id(uuid_info)
                    })
                })
            })
        })
        .await?;

    if let Some(mci) = serenity::CollectComponentInteraction::new(ctx)
        .channel_id(ctx.channel_id())
        .timeout(std::time::Duration::from_secs(120))
        .filter(move |mci| mci.data.custom_id == uuid_info.to_string())
        .await
    {
        mci.create_interaction_response(ctx, |ir| {
            ir.kind(serenity::InteractionResponseType::UpdateMessage)
                .interaction_response_data(|d| {
                    d.embed(|e| {
                        e.title(format!("Added by {}", user.name));

                        e.timestamp(
                            serenity::Timestamp::from_unix_timestamp(
                                time.unwrap_or_default().timestamp(),
                            )
                            .unwrap_or_else(|_| serenity::Timestamp::now()),
                        )
                    });

                    d.set_components(serenity::CreateComponents::default())
                })
        })
        .await?;
    } else {
        message
            .edit(ctx, |m| {
                m.components(|c| {
                    c.create_action_row(|ar| {
                        ar.create_button(|b| {
                            b.label("Info")
                                .style(serenity::ButtonStyle::Primary)
                                .disabled(true)
                                .custom_id(uuid_info)
                        })
                    })
                })
            })
            .await?;
    }

    Ok(())
}
