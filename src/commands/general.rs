use std::{sync::Arc, vec};

use crate::{commands::voice::autocomplete_quote, Context, Error};
use futures::{Stream, StreamExt};
use poise::serenity_prelude::{self as serenity, AttachmentType};

async fn autocomplete_wisdom<'a>(
    ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    let guild_id = ctx.guild_id().unwrap();
    let wisdoms = Arc::clone(&ctx.data().wisdoms);
    let wisdoms = wisdoms.read().await;
    let wisdoms = wisdoms.get(&guild_id.0).map_or(vec![], |x| x.to_owned());

    futures::stream::iter(wisdoms).filter(move |phrase| {
        futures::future::ready(phrase.to_lowercase().contains(&partial.to_lowercase()))
    })
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Infinite wisdom"),
    guild_only
)]
pub async fn wisdom(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_wisdom"] wisdom: Option<String>,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap();

    let wisdom = if let Some(wisdom) = wisdom {
        sqlx::query!(
            "SELECT user_id, phrase, time FROM wisdom WHERE guild_id = $1 AND LOWER(phrase) LIKE $2 LIMIT 1",
            &i64::from(guild_id),
            format!("%{}%", &wisdom.to_lowercase()),
        )
        .map(|r| (r.user_id, r.phrase, r.time))
        .fetch_optional(&pool)
        .await?
    } else {
        sqlx::query!(
            "SELECT user_id, phrase, time FROM wisdom WHERE guild_id = $1 ORDER BY RANDOM () LIMIT 1",
            &i64::from(guild_id),
        )
        .map(|r| (r.user_id, r.phrase, r.time))
        .fetch_optional(&pool)
        .await?
    };

    let (user_id, phrase, time) = match wisdom {
        Some(wisdom) => wisdom,
        None => {
            ctx.say("I can't find that one").await?;
            return Ok(());
        }
    };

    let user = ctx.http().get_user(user_id as u64).await?;

    let uuid_info = ctx.id();
    let message = ctx
        .send(|f| {
            f.content(phrase).components(|c| {
                if user_id == 0 {
                    return c;
                }
                c.create_action_row(|ar| {
                    ar.create_button(|b| {
                        b.style(serenity::ButtonStyle::Primary)
                            .label("Info")
                            .custom_id(uuid_info)
                    })
                })
            })
        })
        .await?;

    if let Some(mci) = serenity::CollectComponentInteraction::new(ctx)
        .channel_id(ctx.channel_id())
        .timeout(std::time::Duration::from_secs(120))
        .filter(move |mci| mci.data.custom_id == uuid_info.to_string())
        .await
    {
        mci.create_interaction_response(ctx, |ir| {
            ir.kind(serenity::InteractionResponseType::UpdateMessage)
                .interaction_response_data(|d| {
                    d.embed(|e| {
                        e.title(format!("Added by {}", user.name));

                        e.timestamp(
                            serenity::Timestamp::from_unix_timestamp(
                                time.unwrap_or_default().timestamp(),
                            )
                            .unwrap_or_else(|_| serenity::Timestamp::now()),
                        )
                    });

                    d.set_components(serenity::CreateComponents::default())
                })
        })
        .await?;
    } else {
        message
            .edit(ctx, |m| {
                m.components(|c| {
                    c.create_action_row(|ar| {
                        ar.create_button(|b| {
                            b.label("Info")
                                .style(serenity::ButtonStyle::Primary)
                                .disabled(true)
                                .custom_id(uuid_info)
                        })
                    })
                })
            })
            .await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Add things"),
    guild_only,
    subcommands("add_wisdom", "add_quote")
)]
pub async fn add(_ctx: Context<'_>) -> Result<(), Error> {
    Ok(())
}

#[derive(poise::Modal)]
#[name = "Add a wisdom"]
struct WisdomModal {
    #[paragraph]
    wisdom: String,
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Create a wisdom"),
    guild_only,
    rename = "wisdom"
)]
pub async fn add_wisdom(ctx: Context<'_>) -> Result<(), Error> {
    use poise::Modal as _;

    let wisdom = if let Context::Application(ctx) = ctx {
        let data = if let Some(data) = WisdomModal::execute(ctx).await? {
            data
        } else {
            return Ok(());
        };
        data.wisdom
    } else {
        return Ok(());
    };

    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap().0 as i64;
    let user_id = ctx.author().id.0 as i64;
    let timestamp = ctx.created_at().naive_utc();

    let result = sqlx::query!(
        "INSERT INTO wisdom (guild_id, user_id, phrase, time) values ($1, $2, $3, $4) ON CONFLICT DO NOTHING",
        &guild_id, &user_id, &wisdom, &timestamp
    )
    .execute(&pool)
    .await?;

    if result.rows_affected() == 0 {
        ctx.say(format!("`{}` is already a wisdom", wisdom)).await?;
        return Ok(());
    }

    if wisdom.len() <= 100 {
        let wisdoms = Arc::clone(&ctx.data().wisdoms);
        let mut wisdoms = wisdoms.write().await;
        let wisdoms = wisdoms.entry(guild_id as u64).or_insert_with(Vec::new);
        wisdoms.push(wisdom.clone());
    }

    ctx.say(format!("Added `{}`", wisdom)).await?;
    let uuid_remove = ctx.id();
    let message = ctx
        .send(|f| {
            f.components(|c| {
                c.create_action_row(|ar| {
                    ar.create_button(|b| {
                        b.style(serenity::ButtonStyle::Danger)
                            .label("Remove")
                            .custom_id(uuid_remove)
                    })
                })
            })
            .ephemeral(true)
        })
        .await?;

    if let Some(mci) = serenity::CollectComponentInteraction::new(ctx)
        .author_id(ctx.author().id)
        .channel_id(ctx.channel_id())
        .timeout(std::time::Duration::from_secs(120))
        .filter(move |mci| mci.data.custom_id == uuid_remove.to_string())
        .await
    {
        let result = sqlx::query!(
            "DELETE FROM wisdom WHERE guild_id = $1 AND phrase = $2;",
            &guild_id,
            &wisdom
        )
        .execute(&pool)
        .await?;

        let status = if result.rows_affected() == 0 {
            "Failed"
        } else {
            if wisdom.len() <= 100 {
                let wisdoms = Arc::clone(&ctx.data().wisdoms);
                let mut wisdoms = wisdoms.write().await;
                let wisdoms = wisdoms.entry(guild_id as u64).or_insert_with(Vec::new);
                if let Some(index) = wisdoms.iter().position(|x| *x == wisdom) {
                    wisdoms.remove(index);
                }
            }
            "Removed"
        };

        ctx.say(status).await?;

        mci.create_interaction_response(ctx, |ir| {
            ir.kind(serenity::InteractionResponseType::UpdateMessage)
                .interaction_response_data(|d| {
                    d.components(|c| {
                        c.create_action_row(|ar| {
                            ar.create_button(|b| {
                                b.label(status)
                                    .style(serenity::ButtonStyle::Danger)
                                    .disabled(true)
                                    .custom_id(uuid_remove)
                            })
                        })
                    })
                })
        })
        .await?;
    } else {
        message
            .edit(ctx, |m| {
                m.components(|c| {
                    c.create_action_row(|ar| {
                        ar.create_button(|b| {
                            b.label("Remove")
                                .style(serenity::ButtonStyle::Danger)
                                .disabled(true)
                                .custom_id(uuid_remove)
                        })
                    })
                })
            })
            .await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Create a quote"),
    guild_only,
    rename = "quote"
)]
pub async fn add_quote(
    ctx: Context<'_>,
    name: String,
    file: serenity::Attachment,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap().0 as i64;
    let user_id = ctx.author().id.0 as i64;
    let timestamp = ctx.created_at().naive_utc();
    let url = file.url;
    let file_name = format!("{} - {}", url.split('/').last().unwrap(), ctx.id()); //lazy
    std::fs::create_dir_all("./data/quotes")?;

    let result = sqlx::query!("SELECT user_id, name, url, time FROM quote WHERE guild_id = $1 AND LOWER(name) LIKE $2 AND url = $3 LIMIT 1",
            &i64::from(guild_id),
            name,
            file_name
        )
    .fetch_optional(&pool)
    .await?;

    if result.is_some() {
        ctx.say(format!("`{}` is already a quote", name)).await?;
        return Ok(());
    }

    let response = reqwest::get(url.clone()).await?;
    let mut file = std::fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(format!("./data/quotes/{}", file_name))?;
    let mut content = std::io::Cursor::new(response.bytes().await?);
    std::io::copy(&mut content, &mut file)?;

    sqlx::query!(
        "INSERT INTO quote (guild_id, user_id, url, name, time) values ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING",
        &guild_id, &user_id, &file_name, &name, &timestamp
    )
    .execute(&pool)
    .await?;

    if name.len() <= 100 {
        let quotes = Arc::clone(&ctx.data().quotes);
        let mut quotes = quotes.write().await;
        let quotes = quotes.entry(guild_id as u64).or_default();
        quotes.push(name.clone());
    }

    ctx.say(format!("Added `{}`", name)).await?;
    let uuid_remove = ctx.id();
    let message = ctx
        .send(|f| {
            f.components(|c| {
                c.create_action_row(|ar| {
                    ar.create_button(|b| {
                        b.style(serenity::ButtonStyle::Danger)
                            .label("Remove")
                            .custom_id(uuid_remove)
                    })
                })
            })
            .ephemeral(true)
        })
        .await?;

    if let Some(mci) = serenity::CollectComponentInteraction::new(ctx)
        .author_id(ctx.author().id)
        .channel_id(ctx.channel_id())
        .timeout(std::time::Duration::from_secs(120))
        .filter(move |mci| mci.data.custom_id == uuid_remove.to_string())
        .await
    {
        let result = sqlx::query!(
            "DELETE FROM quote WHERE guild_id = $1 AND name = $2;",
            &guild_id,
            &name
        )
        .execute(&pool)
        .await?;

        let status = if result.rows_affected() == 0 {
            "Failed"
        } else {
            if name.len() <= 100 {
                let quotes = Arc::clone(&ctx.data().quotes);
                let mut quotes = quotes.write().await;
                let quotes = quotes.entry(guild_id as u64).or_insert_with(Vec::new);
                if let Some(index) = quotes.iter().position(|x| *x == name) {
                    quotes.remove(index);
                }
            }
            "Removed"
        };

        ctx.say(status).await?;

        mci.create_interaction_response(ctx, |ir| {
            ir.kind(serenity::InteractionResponseType::UpdateMessage)
                .interaction_response_data(|d| {
                    d.components(|c| {
                        c.create_action_row(|ar| {
                            ar.create_button(|b| {
                                b.label(status)
                                    .style(serenity::ButtonStyle::Danger)
                                    .disabled(true)
                                    .custom_id(uuid_remove)
                            })
                        })
                    })
                })
        })
        .await?;
    } else {
        message
            .edit(ctx, |m| {
                m.components(|c| {
                    c.create_action_row(|ar| {
                        ar.create_button(|b| {
                            b.label("Remove")
                                .style(serenity::ButtonStyle::Danger)
                                .disabled(true)
                                .custom_id(uuid_remove)
                        })
                    })
                })
            })
            .await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Remove stuff"),
    default_member_permissions = "ADMINISTRATOR",
    guild_only,
    subcommands("rm_wisdom", "rm_quote")
)]
pub async fn remove(_ctx: Context<'_>) -> Result<(), Error> {
    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Remove a wisdom"),
    guild_only,
    rename = "wisdom"
)]
pub async fn rm_wisdom(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_wisdom"] wisdom: String,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap().0 as i64;

    let result = sqlx::query!(
        "DELETE FROM wisdom WHERE guild_id = $1 AND phrase = $2;",
        &guild_id,
        &wisdom,
    )
    .execute(&pool)
    .await?;

    if result.rows_affected() == 0 {
        ctx.say(format!("`{}` is not a wisdom", wisdom)).await?;
        return Ok(());
    }

    if wisdom.len() <= 100 {
        let wisdoms = Arc::clone(&ctx.data().wisdoms);
        let mut wisdoms = wisdoms.write().await;
        let wisdoms = wisdoms.entry(guild_id as u64).or_insert_with(Vec::new);
        if let Some(index) = wisdoms.iter().position(|x| *x == wisdom) {
            wisdoms.remove(index);
        }
    }

    ctx.say(format!("Removed `{}`", wisdom)).await?;

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Remove a quote"),
    guild_only,
    rename = "quote"
)]
pub async fn rm_quote(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_quote"] name: String,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap().0 as i64;

    let result = sqlx::query!(
        "DELETE FROM quote WHERE guild_id = $1 AND name = $2;",
        &guild_id,
        &name,
    )
    .execute(&pool)
    .await?;

    if result.rows_affected() == 0 {
        ctx.say(format!("`{}` is not a quote", name)).await?;
        return Ok(());
    }

    if name.len() <= 100 {
        let quotes = Arc::clone(&ctx.data().quotes);
        let mut quotes = quotes.write().await;
        let quotes = quotes.entry(guild_id as u64).or_insert_with(Vec::new);
        if let Some(index) = quotes.iter().position(|x| *x == name) {
            quotes.remove(index);
        }
    }

    ctx.say(format!("Removed `{}`", name)).await?;

    Ok(())
}

#[derive(Debug, poise::ChoiceParameter)]
pub enum CommandChoice {
    Enable,
    Disable,
}

async fn autocomplete_command<'a>(
    ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    let commands = ctx
        .framework()
        .options()
        .commands
        .iter()
        .map(|c| c.name.to_owned())
        .collect::<Vec<String>>();

    futures::stream::iter(commands).filter(move |command| {
        futures::future::ready(command.to_lowercase().contains(&partial.to_lowercase()))
    })
}

#[poise::command(
    slash_command,
    default_member_permissions = "ADMINISTRATOR",
    description_localized("en-US", "Command options"),
    guild_only
)]
pub async fn command(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_command"] name: String,
    choice: CommandChoice,
) -> Result<(), Error> {
    let pool = ctx.data().pool.clone();
    let guild_id = ctx.guild_id().unwrap().0 as i64;

    match choice {
        CommandChoice::Enable => {
            if let Ok(result) = sqlx::query!(
                "DELETE FROM disabled_commands WHERE guild_id = $1 AND command = $2",
                &guild_id,
                &name
            )
            .execute(&pool)
            .await
            {
                if result.rows_affected() == 0 {
                    ctx.say("Command is not disabled").await?;
                } else {
                    ctx.say(format!("Enabled {}", name)).await?;
                }
            }
        }
        CommandChoice::Disable => {
            if let Ok(result) = sqlx::query!(
                "INSERT INTO disabled_commands (guild_id, command) values ($1, $2) ON CONFLICT DO NOTHING",
                &guild_id,
                &name
            )
            .execute(&pool)
            .await
            {
                if result.rows_affected() == 0 {
                    ctx.say("Command is already disabled").await?;
                } else {
                    ctx.say(format!("Disabled {}", name)).await?;
                }
            }
        }
    }

    Ok(())
}

#[poise::command(
    slash_command,
    context_menu_command = "Avatar",
    description_localized("en-US", "Send the users avatar"),
    guild_only
)]
pub async fn avatar(ctx: Context<'_>, user: serenity::User) -> Result<(), Error> {
    ctx.say(
        user.avatar_url()
            .unwrap_or_else(|| "No avatar found.".to_owned()),
    )
    .await?;
    Ok(())
}

#[poise::command(
    slash_command,
    default_member_permissions = "MOVE_MEMBERS",
    description_localized("en-US", "Boop all members with a specific role"),
    guild_only
)]
pub async fn boop(ctx: Context<'_>, role: serenity::Role) -> Result<(), Error> {
    let guild = ctx.guild().unwrap();
    let members: Vec<serenity::Member> = guild
        .voice_states
        .into_iter()
        .filter_map(|(_, v)| v.member)
        .filter(|m| m.roles.contains(&role.id))
        .collect();

    for member in members {
        member.disconnect_from_voice(&ctx).await?;
    }

    ctx.say("Boop").await?;

    Ok(())
}

#[poise::command(
    slash_command,
    rename = "server-info",
    description_localized("en-US", "Information about the server"),
    guild_only
)]
pub async fn server_info(ctx: Context<'_>) -> Result<(), Error> {
    let guild = ctx.guild().unwrap();
    let created = ctx.guild_id().unwrap().created_at();

    ctx.send(|m| {
        m.embed(|e| {
            e.title(guild.name)
                .field("Owner", format!("<@{}>", guild.owner_id), false)
                .field("Members", guild.member_count, true)
                .field("Created", created.format("%b %e %Y at %r"), true)
        })
    })
    .await?;

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Google images"),
    guild_only
)]
pub async fn img(ctx: Context<'_>, search: String) -> Result<(), Error> {
    let key =
        std::env::var("GOOGLE_API_KEY").expect("Expected a GOOGLE_API_KEY in the environment");
    let cx = std::env::var("GOOGLE_CX").expect("Expected a GOOGLE_CX in the environment");

    if search.contains("safe=") {
        ctx.say("NOT SAFE").await?;
        return Ok(());
    }

    let endpoint = &format!("https://www.googleapis.com/customsearch/v1?key={}&cx={}&searchType=image&num=10&safe=active&q={}", key, cx, search);
    let search_results = reqwest::Client::new()
        .get(endpoint)
        .send()
        .await?
        .json::<serde_json::Value>()
        .await?;

    let item = if let Some(items) = search_results["items"].as_array() {
        items.iter().filter_map(|i| i["link"].as_str()).next()
    } else {
        ctx.say("No results").await?;
        return Ok(());
    };

    ctx.say(item.unwrap_or("No results")).await?;

    Ok(())
}

#[poise::command(
    slash_command,
    context_menu_command = "Info",
    rename = "user-info",
    description_localized("en-US", "Information about the server"),
    guild_only
)]
pub async fn user_info(ctx: Context<'_>, user: serenity::User) -> Result<(), Error> {
    let guild = ctx.guild().unwrap();
    let member = guild.member(&ctx, user).await?;
    let role = member.highest_role_info(ctx);

    ctx.send(|m| {
        m.embed(|e| {
            e.author(|a| {
                a.name(&member.user.name)
                    .icon_url(member.user.avatar_url().unwrap_or_else(|| "".to_owned()))
            });

            if let Some(time) = member.joined_at {
                e.field("Joined", time.format("%b %e %Y at %r"), false);
            }

            if let Some((roleid, _)) = role {
                if let Some(role) = roleid.to_role_cached(ctx) {
                    e.color(role.colour).field("Highest role", role, true);
                }
            }

            e
        })
    })
    .await?;

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Clear the channel"),
    guild_only
)]
pub async fn clear(ctx: Context<'_>) -> Result<(), Error> {
    ctx.say("\n".repeat(1998)).await?;
    Ok(())
}

#[poise::command(
    slash_command,
    default_member_permissions = "ADMINISTRATOR",
    rename = "move-to",
    description_localized("en-US", "Move to a new voice channel"),
    guild_only
)]
pub async fn move_to(ctx: Context<'_>, channel: serenity::GuildChannel) -> Result<(), Error> {
    let guild = ctx.guild().unwrap();

    if channel.kind != serenity::ChannelType::Voice {
        ctx.say("Must be a voice channel").await?;
        return Ok(());
    }

    let member_channel = if let Some(channel) = guild
        .voice_states
        .get(&ctx.author().id)
        .and_then(|vs| vs.channel_id)
    {
        channel
    } else {
        ctx.say("You are not in a voice channel").await?;
        return Ok(());
    };

    let members: Vec<&serenity::Member> = guild
        .voice_states
        .into_iter()
        .filter_map(|(u, v)| {
            if let Some(v_channel_id) = v.channel_id {
                if v_channel_id == member_channel {
                    return guild.members.get(&u);
                }
            }

            None
        })
        .collect();

    ctx.say(format!("Moving to {}", channel.name())).await?;

    for member in members {
        member.move_to_voice_channel(&ctx, &channel).await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    description_localized("en-US", "Delete messages"),
    default_member_permissions = "MANAGE_MESSAGES",
    guild_only
)]
pub async fn wumbo(ctx: Context<'_>, amount: u64) -> Result<(), Error> {
    ctx.say("Deleting messages").await?;
    let messages = ctx
        .channel_id()
        .messages(&ctx, |m| m.limit((amount + 1).clamp(2, 100)))
        .await?;
    ctx.channel_id().delete_messages(&ctx, messages).await?;

    Ok(())
}

#[poise::command(
    prefix_command,
    slash_command,
    subcommands("names_set", "names_generate")
)]
pub async fn names(_ctx: Context<'_>) -> Result<(), Error> {
    Ok(())
}

#[derive(serde::Deserialize, serde::Serialize)]
struct Name {
    uid: u64,
    username: String,
    nickname: Option<String>,
}

#[poise::command(
    slash_command,
    rename = "set",
    description_localized("en-US", "Read a csv with user ids and names to change nicknames"),
    default_member_permissions = "ADMINISTRATOR",
    guild_only
)]
pub async fn names_set(ctx: Context<'_>, file: serenity::Attachment) -> Result<(), Error> {
    let data = file.download().await?;
    let mut rdr = csv::Reader::from_reader(data.as_slice());

    let guild = ctx.guild().unwrap();
    let mut failed_names = vec![];

    ctx.defer().await?;

    for result in rdr.deserialize() {
        let record: Name = result?;
        if let Some(name) = record.nickname {
            let member = guild.member(&ctx, record.uid).await?;
            if let Some(nick) = &member.nick {
                if nick == &name {
                    continue;
                }
            }
            if let Err(_) = member.edit(&ctx, |m| m.nickname(name)).await {
                failed_names.push(member.user.to_string());
            }
        }
    }

    if !failed_names.is_empty() {
        ctx.say(format!(
            "Failed to change some names\n{}",
            failed_names.join("\n")
        ))
        .await?;
    } else {
        ctx.say("Names changed.").await?;
    }

    Ok(())
}

#[poise::command(
    slash_command,
    rename = "generate",
    description_localized("en-US", "Create a template csv to use with the names set command"),
    default_member_permissions = "ADMINISTRATOR",
    guild_only
)]
pub async fn names_generate(ctx: Context<'_>) -> Result<(), Error> {
    let guild = ctx.guild().unwrap();

    let mut wtr = csv::Writer::from_writer(vec![]);
    for member in guild.members(&ctx, None, None).await? {
        wtr.serialize(Name {
            uid: member.user.id.into(),
            username: member.user.name,
            nickname: member.nick,
        })?;
    }

    let data = std::borrow::Cow::from(wtr.into_inner()?);

    ctx.send(|m| {
        m.attachment(AttachmentType::Bytes {
            data,
            filename: format!("{} names.csv", guild.name),
        })
    })
    .await?;

    Ok(())
}
