mod commands;
use commands::*;

use futures::TryStreamExt;
use poise::serenity_prelude as serenity;
use songbird::SerenityInit;
use std::{collections::HashMap, env::var, sync::Arc, time::Duration};
use tokio::sync::RwLock;
// Types used by all command functions
type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

// Custom user data passed to all command functions
pub struct Data {
    wisdoms: Arc<RwLock<HashMap<u64, Vec<String>>>>,
    quotes: Arc<RwLock<HashMap<u64, Vec<String>>>>,
    stars: Arc<RwLock<HashMap<u64, (Option<u64>, u32)>>>,
    pool: sqlx::Pool<sqlx::Postgres>,
}

/// Show this help menu
#[poise::command(prefix_command, owners_only, track_edits, slash_command)]
async fn help(
    ctx: Context<'_>,
    #[description = "Specific command to show help about"]
    #[autocomplete = "poise::builtins::autocomplete_command"]
    command: Option<String>,
) -> Result<(), Error> {
    poise::builtins::help(
        ctx,
        command.as_deref(),
        poise::builtins::HelpConfiguration {
            show_context_menu_commands: true,
            ..Default::default()
        },
    )
    .await?;
    Ok(())
}

/// Registers or unregisters application commands in this guild or globally
#[poise::command(prefix_command, hide_in_help)]
async fn register(ctx: Context<'_>) -> Result<(), Error> {
    poise::builtins::register_application_commands_buttons(ctx).await?;

    Ok(())
}

#[tokio::main]
async fn main() {
    env_logger::init();
    dotenv::dotenv().expect("Failed to load .env file");
    let songbird = songbird::Songbird::serenity();

    let pool = sqlx::postgres::PgPoolOptions::new()
        .connect(&std::env::var("DATABASE_URL").expect("DATABASE_URL env"))
        .await
        .expect("Failed to connect to database");

    if let Err(e) = sqlx::migrate!().run(&pool).await {
        eprintln!("{}", e);
        return;
    }

    let mut data = Data {
        wisdoms: Arc::new(RwLock::new(HashMap::new())),
        quotes: Arc::new(RwLock::new(HashMap::new())),
        stars: Arc::new(RwLock::new(HashMap::new())),
        pool: pool.clone(),
    };

    {
        let rows = sqlx::query!("SELECT guild_id, name FROM quote",)
            .fetch_all(&pool)
            .await
            .unwrap();

        let mut quotes = HashMap::default();

        for row in rows {
            let gquotes = quotes.entry(row.guild_id as u64).or_insert_with(Vec::new);
            if row.name.len() <= 100 {
                gquotes.push(row.name);
            }
        }

        data.quotes = Arc::new(RwLock::new(quotes));

        let rows = sqlx::query!("SELECT guild_id, phrase FROM wisdom",)
            .fetch_all(&pool)
            .await
            .unwrap();

        let mut wisdoms = HashMap::default();

        for row in rows {
            let wisdoms = wisdoms.entry(row.guild_id as u64).or_insert_with(Vec::new);
            if row.phrase.len() <= 100 {
                wisdoms.push(row.phrase);
            }
        }

        data.wisdoms = Arc::new(RwLock::new(wisdoms));
    }

    let options = poise::FrameworkOptions {
        commands: vec![
            help(),
            register(),
            voice::p(),
            voice::pq(),
            voice::queue(),
            voice::pfile(),
            voice::dc(),
            voice::leave(),
            voice::quote(),
            general::wisdom(),
            general::add(),
            general::remove(),
            general::command(),
            general::avatar(),
            general::boop(),
            general::server_info(),
            general::img(),
            general::user_info(),
            general::move_to(),
            general::wumbo(),
            general::names(),
        ],
        prefix_options: poise::PrefixFrameworkOptions {
            prefix: Some("~".into()),
            edit_tracker: Some(poise::EditTracker::for_timespan(Duration::from_secs(3600))),
            ..Default::default()
        },
        /// This code is run before every command
        pre_command: |ctx| {
            Box::pin(async move {
                println!("Executing command {}...", ctx.command().qualified_name);
            })
        },
        /// This code is run after a command if it was successful (returned Ok)
        post_command: |ctx| {
            Box::pin(async move {
                println!("Executed command {}!", ctx.command().qualified_name);
            })
        },
        command_check: Some(|ctx| {
            Box::pin(async move {
                let pool = ctx.data().pool.clone();
                let command = ctx.invoked_command_name();
                let guild = if let Some(guild) = ctx.guild() {
                    guild
                } else {
                    return Ok(true);
                };

                if ctx.author().id == guild.owner_id {
                    return Ok(true);
                }

                if let Ok(row) = sqlx::query!(
                    "SELECT * FROM disabled_commands WHERE guild_id = $1 AND command = $2",
                    &i64::from(guild.id),
                    &command
                )
                .fetch(&pool)
                .try_next()
                .await
                {
                    if row.is_none() {
                        return Ok(true);
                    }
                };

                ctx.say("Command is disabled").await?;
                Ok(false)
            })
        }),
        event_handler: |ctx, event, _framework, data| {
            Box::pin(async move {
                let reaction = match event {
                    poise::Event::ReactionAdd { add_reaction } => (add_reaction, true),
                    poise::Event::ReactionRemove { removed_reaction } => (removed_reaction, false),
                    _ => return Ok(()),
                };

                if reaction.0.emoji != serenity::ReactionType::Unicode("⭐".into()) {
                    return Ok(());
                }

                let guild_id = if let Some(guild_id) = reaction.0.guild_id {
                    guild_id
                } else {
                    return Ok(());
                };

                let starboard = Arc::clone(&data.stars);
                let mut starboard = starboard.write().await;
                let (star_message_id, stars) = starboard
                    .entry(reaction.0.message_id.0)
                    .or_insert((None, 0));

                if reaction.1 {
                    *stars += 1;
                } else {
                    *stars -= 1;
                }

                let channels = guild_id.channels(&ctx.http).await?;

                let channel =
                    if let Some(channel) = channels.iter().find(|(_, gc)| gc.name == "starboard") {
                        channel
                    } else {
                        return Ok(());
                    };

                let react_message = reaction.0.message(&ctx.http).await?;

                if let Some(message_id) = star_message_id {
                    let mut message = channel.0.message(&ctx.http, *message_id).await?;
                    if *stars == 0 {
                        starboard.remove(&reaction.0.message_id.0);
                        message.delete(&ctx.http).await?;
                    } else if let Some(embed) = message.embeds.first() {
                        let mut embed = serenity::CreateEmbed::from(embed.clone());
                        embed.footer(|f| f.text(format!("⭐ {}", stars)));
                        message.edit(&ctx.http, |m| m.set_embed(embed)).await?;
                    }
                } else if *stars == 3 {
                    let message = channel
                        .1
                        .send_message(&ctx.http, |m| {
                            m.components(|c| {
                                c.create_action_row(|a| {
                                    a.create_button(|b| {
                                        b.label("Original");
                                        b.url(react_message.link());
                                        b.style(serenity::ButtonStyle::Link)
                                    })
                                })
                            });

                            m.embed(|e| {
                                e.title(&react_message.content);
                                e.author(|a| {
                                    if let Some(url) = &react_message.author.avatar_url() {
                                        a.icon_url(url);
                                    }
                                    a.name(&react_message.author.name)
                                });

                                if let Some(colour) = react_message.author.accent_colour {
                                    e.color(colour);
                                }

                                e.timestamp(react_message.timestamp);
                                e.footer(|f| f.text(format!("⭐ {}", stars)));

                                if let Some(a) = react_message.attachments.first() {
                                    if react_message.content.is_empty() {
                                        e.title(&a.filename);
                                    }
                                    if let Some(c) = &a.content_type {
                                        if c.contains("video") {
                                            e.description(c);
                                        } else if c.contains("image") {
                                            e.image(&a.url);
                                        }
                                    }
                                }

                                if let Some(m) = react_message.embeds.first() {
                                    if let Some(t) = &m.thumbnail {
                                        e.image(&t.url);
                                    }

                                    if let Some(i) = &m.image {
                                        e.image(&i.url);
                                    }
                                }

                                e
                            });

                            m
                        })
                        .await?;

                    *star_message_id = Some(message.id.0);
                }

                Ok(())
            })
        },
        ..Default::default()
    };

    poise::Framework::builder()
        .token(
            var("DISCORD_TOKEN")
                .expect("Missing `DISCORD_TOKEN` env var, see README for more information."),
        )
        .setup(move |_ctx, _ready, _framework| Box::pin(async move { Ok(data) }))
        .client_settings(move |f| f.voice_manager_arc(songbird).register_songbird())
        .options(options)
        .intents(
            serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT,
        )
        .run()
        .await
        .unwrap();
}
